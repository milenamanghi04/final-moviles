using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public bool Victory;

    [Header("Lista de Personajes")]
    public List<Personajes> personajes;

    [Header("Menu Pausa")]
    [SerializeField] private GameObject botonPausa;
    [SerializeField] private GameObject menuPausa;

    [Header("Menu Personajes")]
    private int indexPJ;
    [SerializeField] private Image image;
    [SerializeField] private TextMeshProUGUI name;




    private void Start()
    {
        indexPJ = PlayerPrefs.GetInt("JugadorIndex");
        if(indexPJ>personajes.Count -1)
        {
            indexPJ = 0;
        }

        if(menuPausa!=null && botonPausa!=null)
        {
            Reanudar();
        }
    }

    void Update()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            ObjetosJuego();
        }

    }
    private void Awake()
    {
        if(GameManager.Instance == null)
        {
            GameManager.Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //MENU PAUSA
    public void Pausa()
    {
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        menuPausa.SetActive(true);
    }
    public void Reanudar()
    {
        Time.timeScale = 1f;
        botonPausa.SetActive(true);
        menuPausa.SetActive(false);
    }
    public void Reiniciar()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Cerrar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        Application.Quit();
    }

    //MENU ELECCION PERSONAJE

    private void CambiarPantalla()
    {
        PlayerPrefs.SetInt("JugadorIndex", indexPJ);
        image.sprite = personajes[indexPJ].imagen;
        name.text = personajes[indexPJ].nombre;
    }
    public void SiguientePersonaje()
    {
        if(indexPJ == personajes.Count - 1)
        {
            indexPJ = 0;
        }
        else
        {
            indexPJ += 1;
        }
        CambiarPantalla();
    }

    public void AnteriorPersonaje()
    {
        if (indexPJ == 0)
        {
            indexPJ = personajes.Count - 1;
        }
        else
        {
            indexPJ -= 1;
        }
        CambiarPantalla();
    }

    public void IniciarJuego()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        

    
    }

    private void ObjetosJuego()
    {
    
        botonPausa = GameObject.Find("btnPausar");
        menuPausa = GameObject.Find("MenuPausa");
       

    }
    



}
