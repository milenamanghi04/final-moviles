using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabilidadBackgammon : MonoBehaviour
{
    [SerializeField] private GameObject habilidad;
     private Vector2 posicionAparicion;
    [SerializeField] private Transform controladorPosicion;
    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    
    void Update()
    {
        
    }

    public void LanzarHabilidad()
    {
        posicionAparicion = controladorPosicion.position;
        Instantiate(habilidad, posicionAparicion, Quaternion.identity);
    }

   public void HabilidadAnim()
    {
        animator.SetTrigger("Habilidad");
    }
}
