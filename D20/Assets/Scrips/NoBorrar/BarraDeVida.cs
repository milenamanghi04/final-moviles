using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeVida : MonoBehaviour
{
    public Slider slider;

    private void Start()
    {
        slider = GetComponent<Slider>();
        if (slider == null)
        {
            Debug.LogError("El componente Slider no se encontr� en el GameObject " + gameObject.name);
        }
    }

    public void CambiarVidaMaxima(float vidaMaxima)
    {
        if (slider != null)
        {
            slider.maxValue = vidaMaxima;
        }
        else
        {
            Debug.LogError("El componente Slider no est� asignado en CambiarVidaMaxima");
        }
    }

    public void CambiarVidaActual(float cantidadVida)
    {
        if (slider != null)
        {
            slider.value = cantidadVida;
        }
        else
        {
            Debug.LogError("El componente Slider no est� asignado en CambiarVidaActual");
        }
    }

    public void InicializarBarraDeVida(float cantidadVida)
    {
        CambiarVidaMaxima(cantidadVida);
        CambiarVidaActual(cantidadVida);
    }
}
