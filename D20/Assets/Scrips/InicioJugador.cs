using UnityEngine;

public class InicioJugador : MonoBehaviour
{
    public GameObject child;
    private GameObject newParent;

    private void Start()
    {
        int indexJugador = PlayerPrefs.GetInt("JugadorIndex");

        // Instanciar el personaje
        GameObject instanciado = Instantiate(GameManager.Instance.personajes[indexJugador].personajeJugable, transform.position, Quaternion.identity);

        // Asignar la etiqueta "Player"
        instanciado.tag = "Player";

        // Asignar el instanciado como el nuevo padre
        newParent = instanciado;

        // Realizar la asignación de hijo si es necesario
        if (newParent != null && child != null)
        {
            Hijo();
        }
    }

    private void Hijo()
    {
        if (newParent != null && child != null)
        {
            // Hace que el objeto child sea hijo del objeto newParent
            child.transform.SetParent(newParent.transform);
            Debug.Log("El objeto hijo ha sido asignado al nuevo padre.");
        }
        else
        {
            Debug.LogWarning("Por favor, asigna los objetos en el inspector.");
        }
    }

    void Update()
    {
        if (newParent == null)
        {
            // Intentar encontrar el objeto con la etiqueta "Player"
            GameObject JugadorObject = GameObject.FindWithTag("Player");

            if (JugadorObject != null)
            {
                newParent = JugadorObject;

                if (newParent != null)
                {
                    Hijo();
                    Debug.Log("Jugador encontrado y asignado en Update.");

                    // Dejar de buscar si ya encontraste al jugador
                    enabled = false; // Desactivar este script para no seguir ejecutando Update
                }
                else
                {
                    Debug.LogError("El componente GameObject no se encuentra en el objeto.");
                }
            }
            else
            {
                Debug.LogWarning("Objeto jugador no encontrado. Intentando nuevamente...");
            }
        }
    }
}