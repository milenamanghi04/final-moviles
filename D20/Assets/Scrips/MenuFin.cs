using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuFin : MonoBehaviour
{
   
    [SerializeField] private GameObject Victoria;
    [SerializeField] private GameObject Derrota;
    void Start()
    {
      




        
    }
    private void Awake()
    {
        GameObject gameManager = GameObject.FindWithTag("GameManager");
        GameManager ameManager = gameManager.GetComponent<GameManager>();
    

        if (ameManager.Victory == true)
        {
            Victoria.SetActive(true);
            Derrota.SetActive(false);
        }
        else if (ameManager.Victory == false)
        {
            Victoria.SetActive(false);
            Derrota.SetActive(true);
        }
    }

  
   public void Volver()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 3);
    }
}
