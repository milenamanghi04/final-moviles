//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.SceneManagement;


//public class Jefe : MonoBehaviour
//{
//    private Animator animator;
//    public Rigidbody2D rb2D;
//    public Transform jugador;
//    private bool mirandoDerecha = true;

//    [Header("Vida")]
//    [SerializeField] private float vida;
//    [SerializeField] private BarraDeVida barraDeVida;

//    [Header("Ataque")]
//    [SerializeField] private Transform controladorAtaque;
//    [SerializeField] private float radioAtaque;
//    [SerializeField] private float da�oAtaque;
//    [SerializeField] private float distanciaJugador;



//    public GameObject JugadorObject;




//    void Start()
//    {
//        animator = GetComponent<Animator>();
//        rb2D = GetComponent<Rigidbody2D>();
//        barraDeVida.InicializarBarraDeVida(vida);
//    }

//    void Update()
//    {
//        if (jugador == null)
//        {
//            JugadorObject = GameObject.FindWithTag("Player");
//            if (JugadorObject != null)
//            {
//                jugador = JugadorObject.GetComponent<Transform>();
//                if (jugador != null)
//                {
//                    Debug.Log("Jugador encontrado y asignado en Update.");
//                    //enabled = false;
//                }
//                else
//                {
//                    Debug.LogError("El componente Transform no se encuentra en el objeto.");
//                }
//            }
//            else
//            {
//                Debug.LogWarning("Objeto jugador no encontrado. Intentando nuevamente...");
//            }
//        }

//        MirarJugador();
//        distanciaJugador = Vector2.Distance(transform.position, jugador.position);
//        animator.SetFloat("DistanciaJugador", distanciaJugador);
//    }

//    public void TomarDa�o(float da�o)
//    {
//        vida -= da�o;
//        barraDeVida.CambiarVidaActual(vida);

//        if (vida <= 0)
//        {
//            animator.SetTrigger("Muerte");
//            Muerte();
//        }
//    }

//    private void Muerte()
//    {
//        GameObject gameManager = GameObject.FindWithTag("GameManager");
//        GameManager ameManager = gameManager.GetComponent<GameManager>();
//        ameManager.Victory = true;
//        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
//        Destroy(gameObject);
//    }

//    public void MirarJugador()
//    {
//        if ((jugador.position.x > transform.position.x && !mirandoDerecha) || (jugador.position.x < transform.position.x && mirandoDerecha))
//        {
//            mirandoDerecha = !mirandoDerecha;
//            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
//        }
//    }

//    public void Ataque()
//    {
//        Debug.Log("Intentando atacar");
//        Collider2D[] objetos = Physics2D.OverlapCircleAll(controladorAtaque.position, radioAtaque);
//        foreach (Collider2D colision in objetos)
//        {
//            if (colision.CompareTag("Player"))
//            {
//                colision.GetComponent<CombateJugador>().TomarDa�o(da�oAtaque);
//                Debug.Log("Jugador atacado");
//            }
//        }
//    }

//    private void OnDrawGizmos()
//    {
//        Gizmos.color = Color.red;
//        Gizmos.DrawWireSphere(controladorAtaque.position, radioAtaque);
//    }
//}