
using UnityEngine;

public class RockDropper : MonoBehaviour
{
    public GameObject rockPrefab; // El prefab de la piedra
    public float dropInterval = 2f; // Intervalo de tiempo entre ca�das
    public Transform dropPointLeft; // Punto de ca�da a la izquierda
    public Transform dropPointRight; // Punto de ca�da a la derecha

    private float timeSinceLastDrop = 0f;

    void Update()
    {
        timeSinceLastDrop += Time.deltaTime;

        if (timeSinceLastDrop >= dropInterval)
        {
            DropRock();
            timeSinceLastDrop = 0f;
        }
    }

    void DropRock()
    {
        // Decide aleatoriamente si dejar caer la piedra a la izquierda o a la derecha
        bool dropLeft = Random.value > 0.5f;

        // Instancia la piedra en el punto adecuado
        Transform dropPoint = dropLeft ? dropPointLeft : dropPointRight;
        Instantiate(rockPrefab, dropPoint.position, dropPoint.rotation);
    }
}

