using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personaje : MonoBehaviour
{
    [HideInInspector] public Rigidbody2D rigidbody2D;
    [HideInInspector] public Animator animator;
    public Joystick joystick;

    [Header("Movimiento")]
    [SerializeField] private float runSpeed = 0;
    private float horizontalMove = 0;
    private bool facingRight = true;

    [Header("Salto")]
    [SerializeField] private float forceJump;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] private Transform groundController;
    private bool onGround;

    [Header("Vida")]
    [SerializeField] private float life;
    [SerializeField] private BarraDeVida lifeBar;

    [Header("UI")]
    [SerializeField] private GameObject Interfaz;



    void Start()
    {
        Interfaz.SetActive(true);
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        lifeBar.InicializarBarraDeVida(life);
        GameObject joystickObject = GameObject.Find("Fixed Joystick");
        if (joystickObject != null)
        {
            joystick = joystickObject.GetComponent<Joystick>();

        }
    }

    void Update()
    {
        horizontalMove = joystick.Horizontal;
        Flip(horizontalMove);
        transform.position += new Vector3(horizontalMove, 0, 0) * Time.deltaTime * runSpeed;
        onGround = Physics2D.OverlapCircle(groundController.position, 0.1f, whatIsGround);
        if(onGround && joystick.Vertical >= 0.5)
        {
            Jump();
        }
    }

    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        }
    }

    public void Jump()
    {
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, forceJump);
    }

    public void TomarDa�o(float da�o)
    {
        life -= da�o;
        lifeBar.CambiarVidaActual(life);
        animator.SetTrigger("RecibirDa�o");
        if(life<=0)
        {
            Destroy(gameObject);
        }
    }
}
