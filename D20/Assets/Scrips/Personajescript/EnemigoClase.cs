using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoClase : MonoBehaviour
{
    public Animator animator;
    public Rigidbody2D rb2D;
    public Transform player;
    public GameObject playerObject;
    private bool lookRight = false;

    [Header("Vida")]
    [SerializeField] public float life;
    [SerializeField] private BarraDeVida barLife;

    void Start()
    {
        animator = GetComponent<Animator>(); 
        rb2D = GetComponent<Rigidbody2D>();
        barLife.InicializarBarraDeVida(life);
        
    }

    
    void Update()
    {
        if(player == null)
        {
            playerObject = GameObject.FindWithTag("Player");
            if(playerObject != null)
            {
                player = playerObject.GetComponent<Transform>();
            }
        }

 

    }

    public void TakeDamage(float damage)
    {
        life -= damage;
        barLife.CambiarVidaActual(life);
        
    }

    public void Death()
    {
        GameObject gameManagerObj = GameObject.FindWithTag("GameManager");
        GameManager gameManager = gameManagerObj.GetComponent<GameManager>();
        gameManager.Victory = true;
        Destroy(gameObject);
    }

    public void LookPlayer()
    {
        if((player.position.x > transform.position.x && !lookRight) || (player.position.x < transform.position.x && lookRight))
        {
            lookRight = !lookRight;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        }
    }
}
