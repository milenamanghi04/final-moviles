//using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alxay : MonoBehaviour
{
    private Rigidbody2D rigidbody2D;
    private Animator animator;
    public Joystick joystick;

    [Header("Movimiento")]
    [SerializeField] private float runSpeed = 0;
    private float horizontalMove = 0;
  
    private bool facingRight = true;

    [Header("Salto")]
    [SerializeField] private float forceJump;
    [SerializeField] private LayerMask whatIsGround;
    [SerializeField] public Transform groundController;
    private bool onGround;

    [Header("SaltoExtra")]
    [SerializeField] bool canDoubleJump;

    [Header("Vida")]
    [SerializeField] public float life;
    [SerializeField] private BarraDeVida lifeBar;

    [Header("Dash")]
    [SerializeField] private float dashSpeed = 20f;
    [SerializeField] private float dashTime = 0.2f;
    [SerializeField] private float dashCooldown = 1f;
    private float dashTimeLeft;
    private float lastDashTime;
    private Vector2 dashDirection;


    [Header("Disparo")]
    [SerializeField] private Transform shotController;
    [SerializeField] private GameObject bullet;
    [SerializeField] private float shotCooldown;
    private float timeNextShot;

    [Header("UI")]
    [SerializeField] private GameObject Interfaz;


    void Start()
    {
        Interfaz.SetActive(true);
        rigidbody2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        lifeBar.InicializarBarraDeVida(life);

        GameObject joystickObject = GameObject.Find("Fixed Joystick");
        if(joystickObject != null)
        {
            joystick = joystickObject.GetComponent<Joystick>();
        }
    }

    void Update()
    {
        //salto
        onGround = Physics2D.OverlapCircle(groundController.position, 0.1f, whatIsGround);
        if(onGround)
        {
            canDoubleJump = true;
        }
        if(onGround && joystick.Vertical >= 0.5)
        {
            Jump();
        }
  
        //movimiento horizontal
        horizontalMove = joystick.Horizontal;
        Flip(horizontalMove);
        transform.position += new Vector3(horizontalMove, 0, 0) * Time.deltaTime * runSpeed;
    }

 

    private void Flip(float horizontal)
    {
        if(horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
            facingRight = !facingRight;
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        }
    }

  

    private void Jump()
    {
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, forceJump);
    }

    public void JumpExtra()
    {
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, forceJump);
        canDoubleJump = false;
    }

    public void TomarDa�o(float da�o)
    {
        life -= da�o;
        lifeBar.CambiarVidaActual(life);
        animator.SetTrigger("RecibirDa�o");
        if(life<=0)
        {
            Destroy(gameObject);
        }
    }

    public void Dash()
    {
        StartDash();

        if (dashTimeLeft > 0)
        {
            dashTimeLeft -= Time.deltaTime;
            rigidbody2D.velocity = dashDirection * dashSpeed;
        }
        else if(dashTimeLeft <=0 && rigidbody2D.velocity != Vector2.zero)
        {
            EndDash();
        }
    }

    void StartDash()
    {
        dashTimeLeft = dashTime;
        lastDashTime = Time.time;
        dashDirection = new Vector3(horizontalMove, 0, 0).normalized;
    }
    void EndDash()
    {
        rigidbody2D.velocity = Vector2.zero;
    }

    public void Shoot()
    {
        if(Time.time >= timeNextShot)
        {
            animator.SetTrigger("Disparo");
        }
    
    }

    public void EventShoot()
    {
        Instantiate(bullet, shotController.position, shotController.rotation);
        timeNextShot = Time.time + shotCooldown;
    }
}
