using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Backgammon : Personaje
{
    [Header("Ataque Daga")]
    [SerializeField] private Transform daggerController;
    [SerializeField] private float daggerRadio;
    [SerializeField] private float daggerDamage;
    [SerializeField] private float daggerCooldown;
    [SerializeField] private float timeNextDagger;

    public void Dagger()
    {
        animator.SetTrigger("Golpe");
    }

    public void DaggerEvent()
    {
        Collider2D[] objects = Physics2D.OverlapCircleAll(daggerController.position, daggerRadio);
        foreach (Collider2D colisionador in objects)
        {
            if(colisionador.CompareTag("Enemigo"))
            {
               // colisionador.transform.GetComponent<Jefe>().TomarDa�o(daggerDamage);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(daggerController.position, daggerRadio);
    }



}
