using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Escudo : MonoBehaviour
{
    [SerializeField] private float tiempoDeVida;

    void Start()
    {
        Destroy(gameObject, tiempoDeVida);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
