using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    private Rigidbody2D rb2D;
    private Animator animator;

    [Header("Salto")]
    [SerializeField] private float fuerzaSalto;
    [SerializeField] private LayerMask queEsSuelo;
    [SerializeField] private Vector3 dimensionesCaja;

    [SerializeField] public Transform controladorSuelo;

    private bool enSuelo;
    private bool salto = false;

    public Joystick joystick;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();

        GameObject joystickObject = GameObject.Find("Fixed Joystick");
        if (joystickObject != null)
        {
            joystick = joystickObject.GetComponent<Joystick>();

        }
    }


    void Update()
    {
        enSuelo = Physics2D.OverlapBox(controladorSuelo.position, dimensionesCaja, 0f, queEsSuelo);
        if (joystick.Vertical >= 0.5f)
        {
            salto = true;
        }
    }

    private void FixedUpdate()
    {
        Movimiento(salto);
        salto = false;
    }

    private void Movimiento(bool salto)
    {
        if (salto)
        {
            if (enSuelo)
            {
                mSalto();
            }
        }
    }

    private void mSalto()
    {
        rb2D.AddForce(new Vector2(0f, fuerzaSalto));
        salto = false;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(controladorSuelo.position, dimensionesCaja);
    }
}
