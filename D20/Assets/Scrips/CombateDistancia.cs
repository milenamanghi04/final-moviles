using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombateDistancia : MonoBehaviour
{
    [SerializeField] public Transform controladorDisparo;
    [SerializeField] public GameObject bullet;

    [SerializeField] public float tiempoEntreDisparos;
    public float tiempoSiguienteDisparo;

    private Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();

    }

    
    void Update()
    {

        
    }
    public void Shoot()
    {

        if(Time.time >= tiempoSiguienteDisparo)
        {
            animator.SetTrigger("Disparo");
            
          //  Instantiate(bullet, controladorDisparo.position, controladorDisparo.rotation);
          //tiempoSiguienteDisparo = Time.time + tiempoEntreDisparos;

        }
        
    }

    public void Disparar()
    {
        Instantiate(bullet, controladorDisparo.position, controladorDisparo.rotation);
        tiempoSiguienteDisparo = Time.time + tiempoEntreDisparos;
    }


}
