using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JefePelea : MonoBehaviour
{
    private Animator animator;

    [SerializeField] private float cooldownTimer = 0f;
    public float abilityCooldown = 5f;

    [SerializeField] private int eleccion;

    [SerializeField]  private int CantHabilidad = 3;

    void Start()
    {
        animator = GetComponent<Animator>();
        cooldownTimer = 0f;
    }

   
    void Update()
    {
        cooldownTimer += Time.deltaTime;

        
        if (cooldownTimer >= abilityCooldown)
        {
            eleccion = Random.Range(0, CantHabilidad);
            EleccionHabilidad();
            cooldownTimer = 0f; // Reinicia el temporizador
        }
    
    }

    private void EleccionHabilidad()
    {
        if(eleccion == 0)
        {
            Habilidad();
        }
        else if(eleccion == 1)
        {
            InvocarEjercito();
        }
        else if (eleccion == 2)
        {
            Golpear();
        }

    }

    private void Habilidad()
    {
        animator.SetTrigger("Habilidad");
    }

    private void InvocarEjercito()
    {
        animator.SetTrigger("InvocoEjercito");
    }

    private void Golpear()
    {
        animator.SetTrigger("Caminar");
    }
}

